import json
import logging

import click
from marshmallow import ValidationError

from receipt.mangers import BookManager
from receipt.serializer import BookSerializer
from . import app, db

logger = logging.getLogger(__name__)


@app.cli.command("load")
@click.argument("filepath")
def load(filepath):
    with open(filepath, "r", encoding="UTF-8") as descriptor:
        data = descriptor.read()
        data = json.loads(data)
        schema = BookSerializer()
        book_manager = BookManager(db)
        try:
            validated_data = schema.load(data)
            book_manager.load_receipts(validated_data)
        except ValidationError as err:
            logger.error(f"Ошибка при загрузке {err.messages}")
