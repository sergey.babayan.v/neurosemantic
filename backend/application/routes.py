import logging

from flask import jsonify, Response, make_response
from flask import request
from marshmallow import ValidationError

from receipt.models import Receipt
from receipt.querysets import ReceiptQuerySet
from receipt.serializer import (
    ReceiptSerializer,
    InputSerializer,
)
from . import app

logger = logging.getLogger(__name__)


@app.route("/")
def hello_world():
    return "Hello World!"


@app.route("/list/")
def list_receipts():
    schema = ReceiptSerializer(many=True)
    queryset = ReceiptQuerySet().all()
    data = schema.dump(queryset)
    return jsonify(data)


@app.route("/count/", methods=["POST"])
def count():
    schema = InputSerializer(many=True)
    output_schema = ReceiptSerializer(many=True)
    try:
        validated_data = schema.load(request.json)
    except ValidationError as err:
        logger.info(f"Плохой запрос на подсчёт {request.json}")
        return make_response(jsonify(err.messages), 400)
    queryset = ReceiptQuerySet().filter_by(validated_data)
    res = jsonify(count=queryset.count(), receipts=output_schema.dump(queryset))
    return res
