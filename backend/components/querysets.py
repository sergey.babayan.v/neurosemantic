from sqlalchemy import func


class QuerySet:
    """ Класс отвечающий за надслойку над ORM,
     для выполнение query запросов """

    model = None

    def all(self):
        return self.model.query.all()

    def get_random(self):
        return self.model.query.order_by(func.random()).first()
