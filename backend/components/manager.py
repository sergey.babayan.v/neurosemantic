class Manager:
    """ Класс отвечающий за бизнес логику между моделями """

    model = None

    def __init__(self, db):
        self.db = db

    def edit(self, instance, data):
        assert self.model, "Попытка обновления без модели"
        for attr in data:
            setattr(instance, attr, data[attr])
        return instance

    def init(self, context):
        assert self.model, "Попытка инициализации без модели"
        return self.model(**context)
