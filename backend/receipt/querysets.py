from sqlalchemy import and_, or_

from components.querysets import QuerySet
from .models import Item, Ingredient, Receipt


class IngredientQuerySet(QuerySet):
    model = Ingredient

    def get_by_name(self, name):
        return self.model.query.filter_by(name=name).first()


class ItemQuerySet(QuerySet):
    model = Item

    def get_by(self, receipt, ingredient):
        return self.model.query.filter_by(
            receipt_id=receipt.id, ingredient_id=ingredient.id
        ).first()

    def get_by_receipt(self, receipt):
        return self.model.query.filter_by(receipt_id=receipt.id).all()


class ReceiptQuerySet(QuerySet):
    model = Receipt

    def get_by_name(self, name):
        return self.model.query.filter_by(name=name).first()

    def filter_by(self, items):
        assert items, "Ожидается список хотябы с одним элементом"
        filter_args = list(
            map(
                lambda item: and_(
                    Item.ingredient.has(Ingredient.name == item["name"]),
                    Item.quantity <= item["quantity"],
                ),
                items,
            )
        )
        return self.model.query.filter(
            Receipt.items.any(or_(*filter_args)),
            ~Receipt.items.any(~or_(*filter_args)),
        ).distinct()
