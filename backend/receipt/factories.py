import factory
from factory import fuzzy
from factory.alchemy import SQLAlchemyModelFactory
from sqlalchemy import func

from application import db
from .models import Receipt, Ingredient, Item


class ReceiptFactory(SQLAlchemyModelFactory):
    name = factory.Sequence(lambda n: u"Receipt %d" % n)

    class Meta:
        model = Receipt
        sqlalchemy_session = db.session


class IngredientFactory(SQLAlchemyModelFactory):
    name = factory.Sequence(lambda n: u"Ingredient %d" % n)

    class Meta:
        model = Ingredient
        sqlalchemy_session = db.session


class ItemFactory(SQLAlchemyModelFactory):
    ingredient = factory.LazyFunction(
        lambda: Ingredient.query.order_by(func.random()).first()
    )
    receipt = factory.LazyFunction(
        lambda: Receipt.query.order_by(func.random()).first()
    )
    quantity = fuzzy.FuzzyInteger(0, 999)

    class Meta:
        model = Item
        sqlalchemy_session = db.session
