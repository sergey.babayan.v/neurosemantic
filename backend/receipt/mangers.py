from components.manager import Manager
from .models import Item, Receipt, Ingredient
from .querysets import ItemQuerySet, IngredientQuerySet, ReceiptQuerySet


class IngredientManager(Manager):
    model = Ingredient
    queryset = IngredientQuerySet

    def get_or_create(self, name):
        queryset = self.queryset()
        ingredient = queryset.get_by_name(name)
        if ingredient:
            return ingredient
        ingredient = self.init({"name": name})
        self.db.session.add(ingredient)
        self.db.session.commit()
        return ingredient


class ItemManager(Manager):
    model = Item
    queryset = ItemQuerySet
    ingredient_manager = IngredientManager

    def get_or_init(self, data):
        return (
            data["receipt"].id
            and data["ingredient"].id
            and self.queryset().get_by(data["receipt"], data["ingredient"])
        ) or self.init(data)

    def create_or_update_many(self, receipt, data):
        ingredient_manager = self.ingredient_manager(self.db)
        for item_data in data:
            ingredient_name = item_data.pop("name")
            ingredient = ingredient_manager.get_or_create(ingredient_name)
            item_data["ingredient"] = ingredient
            item_data["receipt"] = receipt
            item = self.get_or_init(item_data)
            if item.id:
                item = self.edit(item, item_data)
            self.db.session.add(item)
        self.db.session.commit()

    def mark_to_delete(self, queryset):
        return self.db.session.delete(queryset)

    def delete_items_by(self, receipt):
        return self.queryset().get_by_receipt(receipt).delete()


class ReceiptManager(Manager):
    model = Receipt
    item_manager = ItemManager
    queryset = ReceiptQuerySet

    def get_or_init(self, receipt_data):
        return self.queryset().get_by_name(receipt_data["name"]) or self.init(
            receipt_data
        )

    def create_or_update_many(self, data):
        item_manager = self.item_manager(self.db)
        for receipt_data in data:
            items = receipt_data.pop("items", [])
            receipt = self.get_or_init(receipt_data)
            if receipt.id:
                item_manager.delete_items_by(receipt)
                receipt = self.edit(receipt, receipt_data)
            self.db.session.add(receipt)
            self.db.session.commit()
            item_manager.create_or_update_many(receipt, items)


class BookManager(Manager):
    def load_receipts(self, data):
        receipts = data["recipes"]
        receipt_manager = ReceiptManager(self.db)
        receipt_manager.create_or_update_many(receipts)
