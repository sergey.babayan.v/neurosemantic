from marshmallow import (
    fields as base_fields,
    validates_schema,
    ValidationError,
    post_load,
)

from application import ma
from .models import Receipt, Item


class ItemLoadSerializer(ma.Schema):
    item = base_fields.String(attribute="name")
    q = base_fields.Integer(attribute="quantity")


class ReceiptsLoadSerializer(ma.Schema):
    name = base_fields.String()
    components = ma.Nested(ItemLoadSerializer, many=True, attribute="items")


class ItemSerializer(ma.ModelSchema):
    name = base_fields.String(attribute="ingredient.name")

    class Meta:
        model = Item
        fields = ("quantity", "name")


class InputSerializer(ma.Schema):
    name = base_fields.String(required=True)
    quantity = base_fields.Integer(required=True)

    @post_load(pass_many=True)
    def validate_length(self, data, **kwargs):
        if self.many and len(data) < 1:
            raise ValidationError("Ожидается хотябы один элемент")
        return data


class ReceiptSerializer(ma.ModelSchema):
    items = ma.Nested(ItemSerializer, many=True)

    class Meta:
        model = Receipt


class BookSerializer(ma.Schema):
    recipes = ma.Nested(ReceiptsLoadSerializer, many=True)
