from application import db


class Ingredient(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True)


class Receipt(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True)


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer)
    receipt_id = db.Column(db.Integer, db.ForeignKey("receipt.id"), nullable=False)
    receipt = db.relationship("Receipt", backref=db.backref("items", lazy=True))
    ingredient_id = db.Column(
        db.Integer, db.ForeignKey("ingredient.id"), nullable=False
    )
    ingredient = db.relationship("Ingredient", backref=db.backref("items", lazy=True))
