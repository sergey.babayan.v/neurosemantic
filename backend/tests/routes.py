from sqlalchemy import func

from receipt.models import Receipt, Item
from tests.base import FlaskTestCase

from receipt.factories import ReceiptFactory, IngredientFactory, ItemFactory
from receipt.querysets import ReceiptQuerySet, IngredientQuerySet, ItemQuerySet
from receipt.serializer import ReceiptSerializer, InputSerializer
from application import db
from tests.serializers import TestInputSerializer


class TestRoutes(FlaskTestCase):
    receipt_count = 5
    items_count = 10
    ingredient_count = 5

    def setUp(self):
        super().setUp()
        for _ in range(self.ingredient_count):
            IngredientFactory()
        for _ in range(self.receipt_count):
            ReceiptFactory()
        for _ in range(self.items_count):
            ItemFactory()

    def test_list(self):
        response = self.app.get("/list/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), self.receipt_count)

    def test_count_for_empty(self):
        response = self.app.post("/count/", json=[], follow_redirects=True)
        self.assertEqual(response.status_code, 400)
        self.assertIn("_schema", response.json)

    def test_for_one_receipt(self):
        receipt = (
            Receipt.query.filter(Receipt.items.any()).order_by(func.random()).first()
        )
        items = ItemQuerySet().get_by_receipt(receipt)
        schema = TestInputSerializer(many=True)
        input_data = schema.dump(items)
        response = self.app.post("/count/", json=input_data)
        self.assertEqual(response.status_code, 200)
        self.assertGreater(len(response.json), 1)
        self.assertIn(receipt.name, str(response.data, encoding="utf-8"))
