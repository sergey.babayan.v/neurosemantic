from marshmallow import fields as base_fields

from receipt.serializer import InputSerializer


class TestInputSerializer(InputSerializer):
    name = base_fields.String(required=True, attribute="ingredient.name")
