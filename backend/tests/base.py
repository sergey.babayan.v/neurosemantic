import os
import unittest

from application import db
from . import app


class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        db.create_all()
        self.db = db

    def tearDown(self):
        db.session.remove()
        db.drop_all()
