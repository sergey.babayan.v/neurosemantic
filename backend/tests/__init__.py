import os

from flask_migrate import Migrate, upgrade

from application import app, db

db.session.remove()
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["TEST_DATABASE_URI"]
app.config["TESTING"] = True
migrate = Migrate(app, db)
with app.app_context():
    upgrade()
