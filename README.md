## Как запустить
### 1. Создайте .env в папке рядом с docker-compose.yml
    FLASK_APP=application
    DATABASE_URI=postgres://postgres@db:5432/main
    TEST_DATABASE_URI=postgres://postgres@db:5432/test
    
### 2. Запустите
    docker-compose up --build -d
 
### 2. Выполните
    docker-compose exec db sh
    psql -U postgres
    CREATE DATABASE main;
    CREATE DATABASE test;
 
 ### 3. Чтобы наполнить базу
    docker-compose exec backend sh
    flask load task.json

 ### 4. Тесты
    docker-compose exec backend sh
    python -m tests
 

